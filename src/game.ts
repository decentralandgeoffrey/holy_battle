
const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const floor = new Entity()
floor.setParent(scene)
const floorShape = new GLTFShape('models/floor/floor.glb')
floor.addComponentOrReplace(floorShape)
const transformFloor = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floor.addComponentOrReplace(transformFloor)
engine.addEntity(floor)

// 2nd floor

const stairs = new Entity()
stairs.setParent(scene)
const stairShape = new GLTFShape('models/Stairs/Stairs.glb')
stairs.addComponentOrReplace(stairShape)
const transformStairs = new Transform({
  position: new Vector3(8.46, 1.98, 14
  ),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(2, 2, 2)
})
stairs.addComponentOrReplace(transformStairs)
engine.addEntity(stairs)

// rotate

export class SimpleRotate implements ISystem {
  update() {
    let transform = sword.getComponent(Transform)
    transform.rotate(Vector3.Up(), 1)
  }
}
engine.addSystem(new SimpleRotate())

// sword
const sword = new Entity()
sword.setParent(scene)
const swordShape = new GLTFShape('models/Sword/mysword.glb')
sword.addComponentOrReplace(swordShape)
const transformSword = new Transform({
  position: new Vector3(8.5, 4.7, 15.2),
  rotation: new Quaternion(0, 0, 179.407, 1),
  scale: new Vector3(1, 1, 1)
})

// add sword to engine    
sword.addComponentOrReplace(transformSword)
engine.addEntity(sword)

// gate
const gate = new Entity()
gate.setParent(scene)
const gateShape = new GLTFShape('models/gate/gate.glb')
gate.addComponentOrReplace(gateShape)
const transformGate = new Transform({
  position: new Vector3(10.001,2.92,1.189),
  rotation: Quaternion.Euler(-2.159,0,0),
  scale: new Vector3(1, 1, 1)
})
gate.addComponentOrReplace(transformGate)
engine.addEntity(sword)



// zombie
let zombie = new Entity()
zombie.addComponent(new Transform({
  position: new Vector3(3, 0, 3)
}))
zombie.addComponent(new GLTFShape("models/ScreamingZombie/ScreamingZombie.glb"))

// Add animations
const animator = new Animator();
let clipScream = new AnimationState("scream")
clipScream.looping = false
animator.addClip(clipScream);
zombie.addComponent(animator);

// add Sound
// Create AudioClip object, holding audio file
let audioScreamClip = new AudioClip("sounds/zombieScream/486309__kp2494__cthulu-monster-roar.mp3")
// Add audioSource component to entity
zombie.addComponent(new AudioSource(audioScreamClip))

// Add click interaction
zombie.addComponent(new OnClick((): void => {
  // stop previous animation
  clipScream.stop()
  //play animation
  clipScream.play()
zombie.getComponent(AudioSource).playOnce()  
}))


// Add zombie to engine
engine.addEntity(zombie)
zombie.setParent(scene)

# Holy Battle

## Introduction
This Decentraland scene was submitted for the assignments 6, 7 and 8 for the VR academy. 

## How to use

### Download the zip file or clone this GitLab repository
### Install the CLI
run the following command:
`npm i -g decentraland´
### To see the scene
run the following command in the command shell when in your folder:
´dcl start´
### what can you do

Zombie onClick animation and sound

Sword Rotation

Transparency in Blender

Texture and UV Map (stairs, gate) 

#Copyright information
Zombie from Character Generator from Autodesk. Animated with Mixamo from Adobe

Screaming sound was created by Kp2494 from [freesound.org]: (https://freesound.org/people/kp2494/sounds/486309/ "This link takes you to freesound.org") 

Mushrooms belongs to DCL Builder.

All the rest is my creation.
[MIT license](LICENSE.md)